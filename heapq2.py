"""
heapq analog with heap elements order/precedence operator support.
This allows implemention of maxheap, minheap or some other heap with custom elements comparison function.

Copyright 2014, Roman Rodionov (http://rr-develop.com)
Software license: "MIT software license". See http://opensource.org/licenses/MIT
"""


__all__ = ['heappush', 'heappop']

def heappush(heap, x, before):
    j = len(heap)
    heap.append(x)
    _heapup(heap, j, before)

def heappop(heap, before):
    n = len(heap) - 1
    res = heap[0]
    if n:
        heap[0] = heap[n]
        del heap[n]
        _heapdown(heap, 0, n, before)
    else:
        del heap[0]
    return res

def _heapup(heap, j, before):
    x = heap[j]
    while j != 0:
        i = (j - 1) >> 1
        parent = heap[i]
        if not before(x, parent):
            break
        heap[j] = parent
        j = i
    heap[j] = x

def _heapdown(heap, i, n, before): #looks little faster than _heapdown2
    x = heap[i]
    j = (i << 1) + 1
    while j < n:
        j2 = j + 1 
        if j2 < n and not before(heap[j], heap[j2]):
            j = j2
        heap[i] = heap[j]
        i = j
        j = (i << 1) + 1
    heap[i] = x
    _heapup(heap, i, before)

def _heapdown2(heap, i, n, before):
    x = heap[i]
    j = (i << 1) + 1
    while j < n:
        j2 = j + 1 
        if j2 < n and not before(heap[j], heap[j2]):
            j = j2
        if not before(heap[j], x):
            break
        heap[i] = heap[j]
        i = j
        j = (i << 1) + 1
    heap[i] = x


#tests +++++++


def test1():
    import random
    import operator
    lst = range(100)
    random.shuffle(lst)
    print 'Shuffled list with 100 items:'
    print lst
    heap = []
    for v in lst:
        heappush(heap, v, operator.__gt__)
    res = []
    while heap:
        res.append(heappop(heap, operator.__gt__))
    print 'This list after maxheap ordering:'
    print res

def test2(hpush, hpop):
    import random
    import sys
    from time import time
    lst = range(10000)
    heap = []
    tpush = 0
    tpop = 0
    for n in range(100):
        random.shuffle(lst)
        t = time()
        for v in lst:
            hpush(heap, v)
        tpush += time() - t
        res = []
        i = 0
        t = time()
        while heap:
            v = hpop(heap)
            assert v == i
            i += 1
        tpop += time() - t
        sys.stdout.write('.')
        sys.stdout.flush()
    print ''
    print 'total push/pop time is %s/%s' % (tpush, tpop)

if __name__ == '__main__':
    import operator
    import heapq
    print ""
    print "Maxheap testing:\n"
    test1()
    print ""
    print "Testing native heapq (which uses optimized C implementation _heapq internally when possible)"
    test2(heapq.heappush, heapq.heappop)
    print ""
    print "Testing this heapq implementation with order/precedence operator (no C optimizations here)"
    test2(lambda heap, x: heappush(heap, x, operator.__lt__), lambda heap: heappop(heap, operator.__lt__))

